package sematec.android.saturdays;

import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;


public class SharedPrefsActivity extends AppCompatActivity implements View.OnClickListener {
    EditText name;
    EditText family;
    ImageView myImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shared_prefs);
        bindViews();

        Picasso.with(this).load("http://irib.ir/assets/slider_images/20170705110720_8510.png").into(myImage);



        findViewById(R.id.save).setOnClickListener(this);

        name.setText(getShared("name", ""));
        family.setText(getShared("family", ""));
    }

    private void bindViews() {
        name = (EditText) findViewById(R.id.name);
        family = (EditText) findViewById(R.id.family);
        myImage = (ImageView) findViewById(R.id.myImage);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.save) {
            setShared("name", name.getText().toString());
            setShared("family", family.getText().toString());
            Toast.makeText(this, "data has been saved", Toast.LENGTH_SHORT).show();
            name.setText("");
            family.setText("");
        }
    }

    private void setShared(String key, String value) {
        PreferenceManager.getDefaultSharedPreferences(this).edit()
                .putString(key, value).apply();
    }

    private String getShared(String key, String defValue) {
        return PreferenceManager.getDefaultSharedPreferences(this)
                .getString(key, defValue);
    }


}
