package sematec.android.saturdays;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

public class AddNewsStudentActivity extends AppCompatActivity implements View.OnClickListener {
    EditText name;
    EditText family;
    Button save;
    Context mContext;
    String nnn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_news_student);
        mContext = this;
        bindViews();




        findViewById(R.id.relativeRedLight).setOnClickListener(this);
        save.setOnClickListener(this);



    }

    void bindViews() {
        name = (EditText) findViewById(R.id.name);
        family = (EditText) findViewById(R.id.family);
        save = (Button) findViewById(R.id.save);
    }


    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.relativeRedLight) {

        } else if (view.getId() == R.id.save) {
            String nameValue = name.getText().toString();
            String familyValue = family.getText().toString();
            String result = nameValue + " " + familyValue;
            Toast.makeText(mContext, result, Toast.LENGTH_LONG).show();

            Intent detailIntent = new Intent(mContext, DetailStudentActivity.class);
            detailIntent.putExtra("name", nameValue);
            detailIntent.putExtra("family", familyValue);
            startActivity(detailIntent);
            finish();

        }
    }
}
