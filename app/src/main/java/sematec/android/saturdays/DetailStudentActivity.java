package sematec.android.saturdays;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import org.w3c.dom.Text;

public class DetailStudentActivity extends AppCompatActivity {
    TextView result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_student);

        result = (TextView) findViewById(R.id.result);


        Intent studentIntent = getIntent();
        String name = studentIntent.getStringExtra("name");
        String family = studentIntent.getStringExtra("family");

        result.setText(name + " " + family);
    }
}
